'use strict';

import React, { Component } from 'react';

import {
    View,
    Text,
    TouchableOpacity
} from 'react-native';

import Login from './Login';

import PokemonList from './components/Pokemonlist';

class Splash extends React.Component {
    _openPage() {
        this.props.navigator.push({
            component: PokemonList
        })
    }
    render() {
        return (
            <View style={{ flex: 1 }}>
                <TouchableOpacity onPress={this._openPage.bind(this) }>
                    <Text style={{ color: '#55ACEE', fontSize: 16 }}>Get Start</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

export default Splash;