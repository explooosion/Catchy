'use strict';

import React, { Component } from 'react';

import {
    View,
    Text,
    TextInput,
    TouchableOpacity,
} from 'react-native';

import Welcome from './Welcome';

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: null,
        }
    }
    _openPage() {
        this.props.navigator.push({
            component: Welcome,
            params: {
                name: this.state.name,
            }
        })
    }
    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center', backgroundColor: '#FFFFFF' }}>
                <Text style={{ marginTop: 50, fontSize: 20 }}>Login Page</Text>
                <TextInput
                    value={this.state.name}
                    onChangeText={name => this.setState({ name }) }
                    placeholder={'Inpur your name'}
                    style={{ height: 50, width: 200 }} />
                <TouchableOpacity onPress={this._openPage.bind(this) }>
                    <Text style={{ color: '#55ACEE', fontSize: 16 }}>Get Start</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

export default Login;