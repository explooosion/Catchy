'use strict';

import React, { Component } from 'react';

import {
    TextInput,
    View,
    Text,
    TouchableOpacity,
    StyleSheet,
} from 'react-native';


// component
import PokemonList from './components/Pokemonlist';

class Welcome extends React.Component {

    render() {
        return (
            <View style={styles.content}>
                <PokemonList />
            </View>
        );
    }
}

var styles = StyleSheet.create({
    content: {
        flex: 1,
    },
});

export default Welcome;