import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Image,
    TextInput,
    TouchableOpacity,
    Modal,
} from 'react-native';

import { Thumbnail } from 'native-base';
import ListItem from '../components/listitem';
import Clear from '../components/clear';

import {
    ModalAccountSetting,
    ModalAccountPost,
    ModalAccountFollow,
    ModalAccountFans,
    ModalAccountLike,
    ModalAccountMessage,
    ModalAccountBuy,
    ModalAccountSale,
    ModalAccountPage,
    ModalAccountContact,
    ModalAccountChangePwd,
    ModalAccountAbout
} from './modalAccount';

var Util = require('../components/Util');
var {DEV} = Util;

export default class Account extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: 'Robby',
            post: 168,
            follow: 132,
            fans: 215,
            modalComponent: 'Like',
            modalVisible: false,
        }
    }

    GetModalComponent() {

        switch (this.state.modalComponent) {
            case 'Setting':
                return <ModalAccountSetting title={ this.state.modalComponent } />;
            case 'Post':
                return <ModalAccountPost title={ this.state.modalComponent } />;
            case 'Follow':
                return <ModalAccountFollow title={ this.state.modalComponent } />;
            case 'Fans':
                return <ModalAccountFans title={ this.state.modalComponent } />;
            case 'Like':
                return <ModalAccountLike title={ this.state.modalComponent } />;
            case 'Message':
                return <ModalAccountMessage title={ this.state.modalComponent } />;
            case 'Buy':
                return <ModalAccountBuy title={ this.state.modalComponent } />;
            case 'Sale':
                return <ModalAccountSale title={ this.state.modalComponent } />;
            case 'Page':
                return <ModalAccountPage title={ this.state.modalComponent } />;
            case 'Contact':
                return <ModalAccountContact title={ this.state.modalComponent } />;
            case 'ChangePwd':
                return <ModalAccountChangePwd title={ this.state.modalComponent } />;
            case 'About':
                return <ModalAccountAbout title={ this.state.modalComponent } />;
        }
    }

    SetModalVisible(visible, component) {
        this.setState({
            modalVisible: visible,
            modalComponent: component,
        });
    }

    Logout() {
        alert('登出完成');
        this.props.func._logout();
    }

    render() {

        return (
            <View style={styles.content}>

                <View style={styles.head}>
                    <Thumbnail
                        size={50}
                        style={styles.headnail}
                        source={require('../img/me.png') } />
                    <Text style={ styles.headname }> @{ this.state.name }</Text>
                    <TouchableOpacity style= {styles.headsetting} onPress={ () => this.SetModalVisible(true, 'Setting') }>
                        <Image
                            style={ styles.headsettingicon }
                            source={ require('../img/setting.png') } />
                    </TouchableOpacity>
                </View>

                <View style={styles.info}>
                    <View style={styles.infobox}>
                        <TouchableOpacity style={styles.infoitem} onPress={ () => this.SetModalVisible(true, 'Post') }>
                            <Text style={styles.infovalue}>{ this.state.post }</Text>
                            <Text style={styles.infoname}>貼文</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.infobox}>
                        <TouchableOpacity style={styles.infoitem} onPress={ () => this.SetModalVisible(true, 'Follow') }>
                            <Text style={styles.infovalue}>{ this.state.follow }</Text>
                            <Text style={styles.infoname}>追蹤中</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.infobox}>
                        <TouchableOpacity style={styles.infoitem} onPress={ () => this.SetModalVisible(true, 'Fans') }>
                            <Text style={styles.infovalue}>{ this.state.fans }</Text>
                            <Text style={styles.infoname}>粉絲</Text>
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={styles.list}>
                    <TouchableOpacity onPress={ () => this.SetModalVisible(true, 'Like') }>
                        <ListItem itemname={ '我Like' } itemicon={ require('../img/like.png') } />
                    </TouchableOpacity>
                    <TouchableOpacity  onPress={ () => this.SetModalVisible(true, 'Message') }>
                        <ListItem itemname={ '我的訊息' } itemicon={ require('../img/chat.png') } />
                    </TouchableOpacity>
                    <TouchableOpacity  onPress={ () => this.SetModalVisible(true, 'Buy') }>
                        <ListItem itemname={ '我買到' } itemicon={ require('../img/bag.png') } />
                    </TouchableOpacity>
                    <TouchableOpacity  onPress={ () => this.SetModalVisible(true, 'Sale') }>
                        <ListItem itemname={ '我賣出' } itemicon={ require('../img/hammer.png') } />
                    </TouchableOpacity>
                </View>

                <Clear />

                <View style={styles.list}>
                    <TouchableOpacity  onPress={ () => this.SetModalVisible(true, 'Page') }>
                        <ListItem itemname={ '頁面順序' } itemicon={ require('../img/list.png') } />
                    </TouchableOpacity>
                    <TouchableOpacity  onPress={ () => this.SetModalVisible(true, 'Contact') }>
                        <ListItem itemname={ '聯絡我們' } itemicon={ require('../img/content.png') } />
                    </TouchableOpacity>
                    <TouchableOpacity  onPress={ () => this.SetModalVisible(true, 'ChangePwd') }>
                        <ListItem itemname={ '更改密碼' } itemicon={ require('../img/house-key.png') } />
                    </TouchableOpacity>
                    <TouchableOpacity  onPress={ () => this.SetModalVisible(true, 'About') }>
                        <ListItem itemname={ '關於' } itemicon={ require('../img/icon_catchy.png') } />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={ () => this.Logout() }>
                        <ListItem itemname={ '登出' } itemicon={ require('../img/logout.png') } />
                    </TouchableOpacity>
                </View>

                <Modal
                    animationType={"slide"}
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={ () => this.SetModalVisible(false) }
                    >
                    { this.GetModalComponent() }
                </Modal>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    content: {
        flex: 1,
        backgroundColor: '#FFFFFF',
    },
    head: {
        flexDirection: 'row',
        height: 60,
    },
    headnail: {
        position: 'absolute',
        top: 10,
        left: 10,
    },
    headname: {
        position: 'absolute',
        top: 20,
        left: 70,
    },
    headsetting: {
        position: 'absolute',
        top: 20,
        right: 20,
    },
    headsettingicon: {
        height: 25,
        width: 25,
    },
    info: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 45,
        marginTop: 5,
        borderTopWidth: 3,
        borderTopColor: '#cccccc',
        borderBottomWidth: 3,
        borderBottomColor: '#cccccc',
    },
    infobox: {
        flex: 1,
        height: 40,
        borderRightWidth: 1,
        borderRightColor: '#cccccc'
    },
    infoitem: {
        alignItems: 'center',
        paddingTop: 5,
    },
    infovalue: {
        fontSize: 10,
        color: '#222222',
    },
    infoname: {
        fontSize: 11,
        color: '#666666'
    },
    list: {
        paddingLeft: 30,
        paddingRight: 30,
    },
});