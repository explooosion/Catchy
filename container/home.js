'use strict';

import React, { Component } from 'react';

import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    ScrollView,
    TouchableOpacity,
} from 'react-native';

import Main from './main';

export default class Home extends Component {

    _login() {
        this.props.navigator.push({
            component: Main,
        })
    }

    render() {
        return (
            <View style={styles.content}>
                <View style={styles.header}>
                    <Text style={styles.title}>Search box</Text>
                </View>
                <TouchableOpacity onPress={ () => this._login() }>
                    <Text>Login</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

var styles = StyleSheet.create({
    content: {
        flex: 1,
    },
    header: {
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: '#E6E6E6',
        marginTop: 5,
        marginBottom: 5,
        alignSelf: 'center',
    },
    scrollview: {
        paddingTop: 50,
    },
    icon: {
        width: 300,
        height: 300,
        alignSelf: 'center',
    },
    title: {
        fontSize: 25,
        fontWeight: 'bold',
    },
});
