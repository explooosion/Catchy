import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Image,
    TouchableOpacity,
} from 'react-native';


var Util = require('../components/Util');
var {DEV} = Util;


export class ModalAccountSetting extends Component {
    render() {
        return (
            <View><Text>{ this.props.title }</Text></View>
        );
    }
}

export class ModalAccountPost extends Component {
    render() {
        return (
            <View><Text>{ this.props.title }</Text></View>
        );
    }
}

export class ModalAccountFollow extends Component {
    render() {
        return (
            <View><Text>{ this.props.title }</Text></View>
        );
    }
}

export class ModalAccountFans extends Component {
    render() {
        return (
            <View><Text>{ this.props.title }</Text></View>
        );
    }
}

export class ModalAccountLike extends Component {
    render() {
        return (
            <View><Text>{ this.props.title }</Text></View>
        );
    }
}

export class ModalAccountMessage extends Component {
    render() {
        return (
            <View><Text>{ this.props.title }</Text></View>
        );
    }
}

export class ModalAccountBuy extends Component {
    render() {
        return (
            <View><Text>{ this.props.title }</Text></View>
        );
    }
}

export class ModalAccountSale extends Component {
    render() {
        return (
            <View><Text>{ this.props.title }</Text></View>
        );
    }
}

export class ModalAccountPage extends Component {
    render() {
        return (
            <View><Text>{ this.props.title }</Text></View>
        );
    }
}

export class ModalAccountContact extends Component {
    render() {
        return (
            <View><Text>{ this.props.title }</Text></View>
        );
    }
}

export class ModalAccountChangePwd extends Component {
    render() {
        return (
            <View><Text>{ this.props.title }</Text></View>
        );
    }
}

export class ModalAccountAbout extends Component {
    constructor(props) {
        super(props);
        this.state = {
            version: '1.2.9'
        };
    }
    render() {
        return (
            <View>
                <View style={styles.head}>
                    <TouchableOpacity>
                        <Image stlyle={styles.back} source={ require('../img/icon_back.png') } />
                    </TouchableOpacity>
                </View>
                <View style={styles.version} >
                    <Text>目前版本：{ this.state.version }</Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    head: {
        height: 50,
        width: 400,
    },
    back: {
        resizeMode: 'stretch',
    },
    version: {
        height: Util.size.height - 40,
        alignItems: 'center',
        justifyContent: 'center'
    },
});
