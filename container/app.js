import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Navigator,
    View,
    Text,
    Image,
    Modal,
    TouchableOpacity,
    ScrollView,
    TextInput,
} from 'react-native';

import ScrollableTabView, {DefaultTabBar, ScrollableTabBar, } from 'react-native-scrollable-tab-view';

import Main from './main';
import Home from './home';
import Account from './account';
import Notice from './notice';

var Util = require('../components/Util');
var {DEV} = Util;


export default class appdemo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalLoginVisible: false,
            isLogin: false,
            modalToolComponent: 'Account',
            modalToolVisible: false,
        }

        this._renderScene = this._renderScene.bind(this);

    }

    // Login Modal
    _setModalLoginVisible(visible) {
        this.setState({
            modalLoginVisible: visible,
        });
    }

    // Tool Modal 
    _setModalToolVisible(visible, component) {
        let islogin = this.state.isLogin;

        // If not login then load login component
        if (!islogin) {
            this._checkLogin(islogin);

        } else {
            this.setState({
                modalToolVisible: visible,
            });

            if (visible) {
                this.setState({
                    modalToolComponent: component,
                });
            }
        }
    }

    // Load Modal
    _getModalToolComponent() {
        switch (this.state.modalToolComponent) {
            case 'Account':
                return <Account func={this} />;
            case 'Notice':
                return <Notice />;
        }
    }

    _logout() {
        this.setState({
            modalLoginVisible: false,
            modalToolVisible: false,
            isLogin: false,
        });
    }

    _checkLogin(pass) {

        if (pass) {
            this.setState({
                modalLoginVisible: false,
                isLogin: true,
            });
        } else {
            this.setState({
                modalLoginVisible: true,
                isLogin: false,
            });
        }

    }

    _renderScene(route, navigator) {
        let Component = route.component;

        return (
            <Component navigator={navigator} {...route.passProps} func={this} />
        );
    }

    _configureScene(route, routeStack) {
        if (route.component == Main) {
            return Navigator.SceneConfigs.HorizontalSwipeJumpFromRight
        }
        return Navigator.SceneConfigs.HorizontalSwipeJump
    }

    render() {

        return (

            <View style={styles.content}>

                <View style={styles.header}>
                    <TextInput
                        style={styles.search}
                        placeholder={'輸入關鍵字尋找'}
                        />
                </View>

                <ScrollableTabView
                    renderTabBar={() => <DefaultTabBar backgroundColor='#FFFFFF' />}
                    tabBarPosition='overlayTop'
                    >
                    <ScrollView tabLabel='問問看' style={styles.scrollview}>
                        <View style={styles.container} >
                            <Text style={{ color: '#f00', marginTop: 10, fontSize: 30, }}>{this.state.isLogin == true ? '已登入' : '未登入'}</Text>
                        </View>
                    </ScrollView>
                    <ScrollView tabLabel='人氣榜' style={styles.scrollview}>
                        <View style={styles.container} >
                            <Text>熱門</Text>
                        </View>
                    </ScrollView>
                    <ScrollView tabLabel='交易區' style={styles.scrollview}>
                        <View style={styles.container} >
                            <Text>拍賣</Text>
                        </View>
                    </ScrollView>
                </ScrollableTabView>

                <View style={styles.footer} >
                    <TouchableOpacity
                        onPress={ () => this._checkLogin(this.state.isLogin) }
                        style={styles.footer_box} >
                        <Image
                            style={ styles.footer_icon }
                            source={ require('../img/icon_home_black.png') } />
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={ () => this._checkLogin(this.state.isLogin) }
                        style={styles.footer_box} >
                        <Image
                            style={ styles.footer_icon }
                            source={ require('../img/icon_search_black.png') } />
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={ () => this._checkLogin(this.state.isLogin) }
                        style={styles.footer_box_plus} >
                        <Image
                            style={ styles.footer_icon }
                            source={ require('../img/icon_plus.png') } />
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={ () => this._setModalToolVisible(true, 'Account') }
                        style={styles.footer_box} >
                        <Image
                            style={ styles.footer_icon }
                            source={ require('../img/icon_user_black.png') } />
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={ () => this._setModalToolVisible(true, 'Notice') }
                        style={styles.footer_box} >
                        <Image
                            style={ styles.footer_icon }
                            source={ require('../img/icon_bell_black.png') } />
                    </TouchableOpacity>
                </View>

                <Modal
                    animationType={"slide"}
                    transparent={false}
                    visible={this.state.modalLoginVisible}
                    onRequestClose={ () => this._setModalLoginVisible(false) }
                    >
                    <Navigator
                        initialRoute={{
                            component: Main,
                            passProps: {
                                login: this.state.isLogin,
                            },
                        }}
                        renderScene={ this._renderScene }
                        configureScene={ this._configureScene } />
                </Modal>

                <Modal
                    transparent={false}
                    visible={this.state.modalToolVisible}
                    onRequestClose={ () => this._setModalToolVisible(false) }
                    >
                    { this._getModalToolComponent() }
                </Modal>

            </View>

        );
    }
}


const styles = StyleSheet.create({
    content: {
        flex: 1,
        backgroundColor: '#FFFFFF',
    },
    header: {
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderBottomColor: '#DDDDDD',
    },
    search: {
        width: Util.size.width * 0.95,
        height: 50,
    },
    scrollview: {
        paddingTop: 50,
    },
    container: {
        alignItems: 'center',
    },
    footer: {
        height: 50,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#DDDDDD',
        borderTopWidth: 1,
        borderTopColor: '#BBBBBB'
    },
    footer_box: {
        alignItems: 'center',
        marginLeft: 15,
        marginRight: 15,
    },
    footer_box_plus: {
        backgroundColor: '#F13862',
        borderRadius: 20,
        padding: 2,
        marginLeft: 20,
        marginRight: 20,
    },
    footer_icon: {
        width: 30,
        height: 30,
        resizeMode: 'contain'
    },
});


AppRegistry.registerComponent('appdemo', () => appdemo);