import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    View,
    Text,
    TextInput,
    Image,
    TouchableOpacity,
    BackAndroid,
} from 'react-native';

import Main from './main';
var Util = require('../components/Util');
var { DEV } = Util;


export default class LoginEmail extends Component {
    constructor(props) {
        super(props);
    }

    _back() {
        this.props.navigator.pop();
    }

    _login() {

        alert('登入成功!');
        this.props.func._checkLogin(true);
    }

    _forgetPwd() {
        alert("所以你忘記密碼?");
    }

    render() {

        const { title } = this.props;

        return (
            <View style={styles.content}>

                <View style={styles.head}>
                    <TouchableOpacity
                        style={ styles.control }
                        onPress = { () => this._back() }>
                        <Image  style={ styles.control_back } source= { require('../img/icon_back.png') } />
                    </TouchableOpacity>
                    <View style={styles.title}>
                        <Text style={styles.titletext} >{ title }</Text>
                    </View>
                </View>

                <View style={styles.form}>
                    <Text style={ styles.form_msg }>log in to your account</Text>
                    <View style={styles.row}>
                        <Image
                            style = { styles.icon }
                            source= { require('../img/icon_email.png') } />
                        <TextInput style={ styles.input } placeholder={ 'email' } />
                    </View>
                    <View style={styles.row}>
                        <Image
                            style = { styles.icon }
                            source= { require('../img/icon_key.png') } />
                        <TextInput secureTextEntry={true} style={ styles.input } placeholder={ 'password' } />
                    </View>
                    <TouchableOpacity
                        onPress={ () => this._login() }
                        style= {styles.button_login} >
                        <Text style = { styles.buttontext_dark }>登入</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress= { () => this._forgetPwd() }>
                        <Text style = { styles.label_forget }>忘記密碼?</Text>
                    </TouchableOpacity>
                </View>

            </View>
        );
    }
}


const styles = StyleSheet.create({
    content: {
        flex: 1,
        backgroundColor: '#FFFFFF',
    },
    head: {
        height: 40,
        flexDirection: 'row',
        alignItems: 'center',
    },
    control: {
        position: 'absolute',
        left: 20,
        top: 8,
    },
    control_back: {
        height: 30,
        width: 30,
    },
    title: {
        flex: 1,
        alignItems: 'center',
    },
    titletext: {
        color: '#444444',
        fontWeight: 'bold',
        fontSize: 20,
    },
    form: {
        alignItems: 'center',
        marginTop: 40,
    },
    form_msg: {
        fontSize: 20,
        color: '#444444',
    },
    form_forget: {
        fontSize: 16,
        color: '#444444',
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        width: Util.size.width / 1.1,
        height: 50,
        marginTop: 15,
        borderWidth: 1,
        borderColor: '#444444',
        borderRadius: 10,
        backgroundColor: '#FFFFFF'
    },
    input: {
        width: Util.size.width / 2,
        paddingLeft: 15,
        paddingRight: 15,
        backgroundColor: '#ffffff',
        fontSize: 16,
    },
    icon: {
        width: 30,
        resizeMode: 'contain',
        marginLeft: 15,
    },
    button_login: {
        width: Util.size.width / 1.2,
        height: 40,
        marginTop: 25,
        marginBottom: 25,
        borderRadius: 6,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#CCCCCC',
    },
    buttontext_dark: {
        fontSize: 15,
        color: '#444444',
    },
    label_forget: {
        fontSize: 15,
        color: '#444444',
    },
});
