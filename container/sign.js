import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    View,
    Text,
    TextInput,
    Image,
    TouchableOpacity,
    PixelRatio,
    BackAndroid,
} from 'react-native';

import { Thumbnail } from 'native-base';

import Main from './main';
var Util = require('../components/Util');
var { DEV } = Util;


export default class Sign extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: null,
            password: null,
            date: null,
            phone: null,
        }
    }

    _back() {
        this.props.navigator.pop();
    }

    _next() {
        alert('go next');
    }

    _formatDate(value) {

        const len = value.length;
        if (len == 4 || len == 7) {
            value = value + '-';
        }
        this.setState({ date: value })
    }

    _formatPhone(value) {
        const len = value.length;
        if (len == 4 || len == 8) {
            value = value + '-';
        }
        this.setState({ phone: value })
    }

    render() {

        return (
            <View style={styles.content}>
                <View style={styles.head}>
                    <TouchableOpacity
                        style={ styles.control_back }
                        onPress = { () => this._back() }>
                        <Image
                            style={styles.control}
                            source= { require('../img/icon_back.png') } />
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={ styles.control_next }
                        onPress = { () => this._next() }>
                        <Image
                            style={styles.control}
                            source= { require('../img/icon_check.png') } />
                    </TouchableOpacity>
                </View>

                <View style={styles.form}>
                    <Image
                        style={ styles.form_title }
                        source={ require('../img/catchy_logo.png') } />
                    <Thumbnail
                        size={100}
                        style={styles.nail}
                        source={require('../img/me.png') } />
                    <View style={styles.row}>
                        <Image
                            style = { styles.icon }
                            source= { require('../img/icon_email_black.png') } />
                        <TextInput style={ styles.input } placeholder={ 'email' } />
                    </View>
                    <View style={styles.row}>
                        <Image
                            style = { styles.icon }
                            source= { require('../img/icon_key_black.png') } />
                        <TextInput secureTextEntry={true} style={ styles.input } placeholder={ 'password' } />
                    </View>
                    <View style={styles.row}>
                        <Image
                            style = { styles.icon }
                            source= { require('../img/icon_at_black.png') } />
                        <TextInput style={ styles.input } placeholder={ 'user name' } />
                    </View>
                    <View style={styles.row}>
                        <Image
                            style = { styles.icon }
                            source= { require('../img/icon_gift_black.png') } />
                        <TextInput
                            style={ styles.input }
                            maxLength = {10}
                            placeholder={ 'YYYY-MM-DD (option)' }
                            value={this.state.date}
                            onChangeText = { (value) => this._formatDate(value) }
                            />
                    </View>
                    <View style={styles.row}>
                        <Image
                            style = { styles.icon }
                            source= { require('../img/icon_phone_black.png') } />
                        <TextInput
                            style={ styles.input }
                            maxLength = {12}
                            placeholder={ 'your phone number (option)' }
                            value={this.state.phone}
                            onChangeText = { (value) => this._formatPhone(value) } />
                    </View>
                </View>

            </View>
        );
    }
}


const styles = StyleSheet.create({
    content: {
        flex: 1,
        backgroundColor: '#FFFFFF'
    },
    head: {
        height: 40,
        flexDirection: 'row',
        alignItems: 'center',
    },
    control: {
        height: 30,
        width: 30,
    },
    control_back: {
        position: 'absolute',
        left: 20,
        top: 8,
    },
    control_next: {
        position: 'absolute',
        right: 20,
        top: 8,
    },
    form: {
        alignItems: 'center',
    },
    form_title: {
        height: 60,
        resizeMode: 'contain',
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        width: Util.size.width / 1.1,
        height: 60,
        borderTopWidth: 1,
        borderTopColor: '#AAAAAA',
    },
    input: {
        width: Util.size.width,
        paddingLeft: 15,
        paddingRight: 15,
        backgroundColor: '#ffffff',
        fontSize: 14,
    },
    nail: {
        marginBottom: 20,
    },
    icon: {
        width: 30,
        resizeMode: 'contain',
        marginLeft: 15,
    },
});
