import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    View,
    Text,
    Image,
    BackAndroid,
} from 'react-native';

import LoginEmail from './loginmail';
import Sign from './sign';

var Util = require('../components/Util');
var { DEV } = Util;

export default class Main extends Component {

    _navLoginMail() {
        this.props.navigator.push({
            component: LoginEmail,
            passProps: {
                login: false,
            },
        })
    }

    _navSign() {
        this.props.navigator.push({
            component: Sign,
        })
    }

    render() {

        return (
            <View style={ styles.container }>
                <Image
                    style= { styles.banner }
                    source= { require('../img/catchy_home_img.png') }
                    />
                <Image
                    style= { styles.banner_sub }
                    source= { require('../img/catchy_home_logo.png') }
                    />
                <TouchableOpacity style= {[styles.button, styles.button_fb]} >
                    <Text style = { styles.buttontext_light }>Facebook 登入</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style= {[styles.button, styles.button_email]}
                    onPress= { () => this._navLoginMail() } >
                    <Text style = { styles.buttontext_light }>email 登入</Text>
                </TouchableOpacity>
                <Text style = { styles.label_alert }>還沒有帳號嗎?</Text>
                <TouchableOpacity
                    style= {[styles.button, styles.button_sign]}
                    onPress ={ () => this._navSign() }>
                    <Text style = { styles.buttontext_dark }>註冊</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
    },
    banner: {
        height: Util.size.height / 3,
        resizeMode: 'contain',
    },
    banner_sub: {
        height: Util.size.height / 7,
        resizeMode: 'contain',
        marginBottom: 30,
    },
    button: {
        width: Util.size.width / 1.1,
        height: 40,
        marginBottom: 15,
        borderRadius: 4,
        alignItems: 'center',
        justifyContent: 'center',
    },
    button_fb: {
        backgroundColor: '#39589B',
    },
    button_email: {
        backgroundColor: '#F13862',
    },
    button_sign: {
        backgroundColor: '#CCCCCC',
    },
    buttontext_light: {
        fontSize: 15,
        color: '#FFFFFF',
    },
    buttontext_dark: {
        fontSize: 15,
        color: '#444444',
    },
    label_alert: {
        marginBottom: 10,
        fontSize: 15,
        color: '#F14262',
    },
});
