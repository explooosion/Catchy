'use strict';

import React, { Component } from 'react';

import {
    ListView,
    StyleSheet,
    Text,
    View,
    Image,
    AsyncStorage,
    PixelRatio,
    Platform,
    Alert,
    TouchableOpacity,
} from 'react-native';

import Welcome from '../Welcome';


var REQUEST_URL = 'http://114.33.108.7/pokemons.json';
var Util = require('./Util');
var {DEV} = Util;

export default class PokemonList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataSource: new ListView.DataSource({
                rowHasChanged: (row1, row2) => row1 !== row2,
            }),
            loaded: false,
            navigator: null,
        };

        this.fetchData = this.fetchData.bind(this);

    }

    componentDidMount() {
        this.fetchData();
    }

    fetchData() {
        fetch(REQUEST_URL)
            .then((response) => response.json())
            .then((responseData) => {
                this.setState({
                    dataSource: this.state.dataSource.cloneWithRows(responseData),
                    loaded: true,
                });
            })
            .done();
    }


    render() {
        if (!this.state.loaded) {
            return this.renderLoadingView();
        }

        return (
            <View style={styles.container}>
                <ListView
                    contentContainerStyle={{
                        flexWrap: 'wrap',
                        flexDirection: 'row'
                    }}
                    pageSize={50}
                    dataSource={this.state.dataSource}
                    renderRow={this.renderPokemon}
                    showsVerticalScrollIndicator={true}>
                </ListView>
            </View >
        );
    }

    renderLoadingView() {
        return (
            <View style={styles.load}>
                <Text style={styles.loadtext}>Loading ...</Text>
            </View>
        );
    }

    renderPokemon(re) {

        return (
            <PokemonItem re={re} />
        );
    }

}

class PokemonItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: false
        }
    }

    rowPressed(propertyId) {
        console.log(propertyId);
        //this.props.navigator.pop();
    }

    render() {
        const { re } = this.props;
        return (
            <View style={styles.cell} >
                <TouchableOpacity onPress={ () => this.rowPressed(re.id) }>
                    <View>
                        <Image
                            style={styles.img}
                            source={{ uri: re.img }}
                            />
                        <Text style={{ color: '#444444', fontSize: 10, marginTop: 2 }}>
                            #{re.num}
                        </Text>
                        <Text style={{ color: '#215AA4', fontSize: 12, fontWeight: 'bold' }}>
                            {re.name}
                        </Text>
                        <Text style={{ color: '#BBBBBB', fontSize: 12 }}>
                            {re.type}
                        </Text>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}

var styles = StyleSheet.create({
    load: {
        height: 300,
        alignItems: 'center',
        flexDirection: 'column',
    },
    loadtext: {
        fontSize: 25,
        textAlign: 'center',
        fontWeight: 'bold',
    },
    container: {
        flex: 1,
    },
    cell: {
        justifyContent: 'center',
        width: Util.size.width / 3,
        height: 140,
        alignItems: 'center',
        //borderWidth: 1 / PixelRatio.get(),
        //borderColor: '#CCC',
    },
    img: {
        alignSelf: 'center',
        width: 80,
        height: 80,
        marginTop: 15,
    },
    wrapper: {
        borderRadius: 5,
    },
});