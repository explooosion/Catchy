'use strict';

import React, { Component } from 'react';

import {
  ListView,
  StyleSheet,
  Text,
  View,
} from 'react-native';

var REQUEST_URL = 'http://opendata.epa.gov.tw/ws/Data/UVIF/?format=json';

export default class UviList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      }),
      loaded: false,
    };
    this.fetchData = this.fetchData.bind(this);
  }

  componentDidMount() {
    this.fetchData();
  }

  fetchData() {
    fetch(REQUEST_URL)
      .then((response) => response.json())
      .then((responseData) => {
        this.setState({
          dataSource: this.state.dataSource.cloneWithRows(responseData),
          loaded: true,
        });
      })
      .done();
  }

  render() {
    if (!this.state.loaded) {
      return this.renderLoadingView();
    }

    //console.log(this.state.dataSource);
    return (
      <View style={styles.content}>
        <View style={styles.container}>
          <Text style={[styles.name, styles.head]}>區域</Text>
          <Text style={[styles.uvi, styles.head]}>等級</Text>
          <Text style={[styles.status, styles.head]}>評等</Text>
          <Text style={[styles.publish, styles.head]}>更新日期</Text>
        </View>
        <ListView
          dataSource={this.state.dataSource}
          renderRow={this.renderUVIF}
          style={styles.listView}
          />
      </View>
    );
  }

  renderLoadingView() {
    return (
      <View style={styles.load}>
        <Text style={styles.loadtext}>Loading ...</Text>
      </View>
    );
  }

  renderUVIF(re) {

    let status = styles.status;
    if (re.UVIStatus == '危險級') {
      status = [styles.status, styles.statusDan];
    }
    //console.log(status);
    return (
      <View style={styles.container}>
        <Text style={styles.name}>{re.Name}</Text>
        <Text style={styles.uvi}>{re.UVI}級</Text>
        <Text style={status}>{re.UVIStatus}</Text>
        <Text style={styles.publish}>{re.PublishTime}</Text>
      </View>
    );
  }
}

var styles = StyleSheet.create({
  load: {
    height: 300,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
  loadtext: {
    fontSize: 25,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  container: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: '#E6E6E6',
  },
  head: {
    fontWeight: 'bold',
  },
  name: {
    flex: 1.5,
    fontSize: 15,
    textAlign: 'left',
    backgroundColor: 'powderblue',
    color: 'white',
    height: 25,
  },
  uvi: {
    flex: 1,
    fontSize: 15,
    textAlign: 'right',
    backgroundColor: 'skyblue',
    color: 'white',
    height: 25,
  },
  status: {
    flex: 1,
    fontSize: 15,
    textAlign: 'center',
    backgroundColor: 'steelblue',
    color: 'white',
    height: 25,
  },
  statusDan: {
    color: 'red',
  },
  publish: {
    flex: 2,
    fontSize: 15,
    textAlign: 'center',
    backgroundColor: 'rgb(47,88,122)',
    color: 'white',
    height: 25,
  },
  listView: {
    flexDirection: 'column',
  },
  content: {
    flex: 1,
    marginBottom: 50,
  },
});