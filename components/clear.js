import React, { Component } from 'react';
import {
    View,
} from 'react-native';

export default class Clear extends Component {
    render() {
        return (
            <View style={{ marginTop: -11, height: 3, backgroundColor: '#bbbbbb' }}></View>
        );
    }
}