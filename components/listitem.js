import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Image,
} from 'react-native';

var Util = require('./Util');
var {DEV} = Util;

export default class ListItem extends Component {

    render() {

        return (
            <View style={styles.row}>
                <Image style={styles.listicon} source={ this.props.itemicon } />
                <View style= { styles.listitem }>
                    <Text style={styles.listtext}>{  this.props.itemname }</Text>
                    <Image style={styles.iconnext} source={ require('../img/icon_next.png') } />
                </View>
            </View >
        );
    }
}

const styles = StyleSheet.create({
    row: {
        flexDirection: 'row',
        margin: 10,
    },
    listicon: {
        width: 16,
        height: 16,
    },
    listitem: {
        flexDirection: 'row',
        width: Util.size.width - 100,
        height: 25,
        marginLeft: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#bbbbbb',
    },
    listtext: {
        fontSize: 14,
    },
    iconnext: {
        position: 'absolute',
        right: 0,
        width: 20,
        height: 20,
    },
});