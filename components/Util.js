'use strict';

import React, { Component } from 'react';

import {
    Alert,
    Platform,
    Dimensions,
    PixelRatio,
    NativeModules,
} from 'react-native';

let Util = {
    DEV: true,
    pixel: 1 / PixelRatio.get(),
    size: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height
    },
    // get the device width and height
};
module.exports = Util;
